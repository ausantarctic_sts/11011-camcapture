#!/usr/bin/env python2.7
"""
Python module intended to provide upstairs component of new camera
functionality

"""

from serialisation import deserialise, CAM_KEY, AUX_KEY
import serial
from datetime import datetime
from time import sleep
from multiprocessing import Process

IN_PORT = 'COM1'
OUT_PORT = 'COM2'
CAMFILE = 'Photolist.txt'
PIC_DELAY = 20

class PicLoop(Process):

    def __init__(self, down_stairs):
        Process.__init__(self)
        self.kill = False
        self.ds = down_stairs

    def run(self):
        while not self.kill:
            sleep(PIC_DELAY)
            self.ds.write('Snap')


class DownStairs(object):

    def __init__(self, port_url):
        """
        Open up the port, and initialise the generator....

        """
        self.port = serial.serial_for_url(port_url)
        self.listener.next()
        self.write = self.port.write

    def listener(self):
        """
        Generator that listens on the port described by port_url and generates n
        bytes of data when an integer n is sent to it.

        """

        length = yield(None)
        while True:
            length = yield(self.port.read(length))

def cam_callback(state, worker):
    if state & not worker.is_alive():
        cam_process = Process(target=picture_loop)
        cam_process.start()
    else:
        cam_process.kill()


def transmit_cam(data):
    """
    Initially data should just be a string with some info about the captured
    images. This simply writes that to a file with a timestamp

    """

    with open(CAMFILE, 'w') as camlog:
        time_str = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
        camlog.write(''.join([time_str, ',', data, '\n']))

if __name__ == "__main__":
    aux_port = serial.serial_for_url(OUT_PORT)
    ds = DownStairs(IN_PORT)

    while True:
        received = deserialise(ds.listener)
        try:
            cam_data = received[CAM_KEY]
            transmit_cam(cam_data)
        except KeyError:
            pass
        try:
            aux_data = received[AUX_KEY]
            aux_port.write(data + '\n')
        except KeyError:
            pass
