#!/usr/bin/env python2.7
"""
Python script that is intended to run forever (basically as a daemon)...
It is intended to listen on two serial ports - aux and upstream.
 -Data that arrives on aux is formatted and sent to upstream.
 -Data that arrives on upstream is intended to trigger some photographic
  function and return some data about how that went.....

"""

import serial
import threading
import subprocess
from serialisation import serialise, PAYLOAD_MAX, CAM_KEY, AUX_KEY

AUX = r'/dev/ttyUSB0'
UPSTREAM = r'/dev/ttyUSB1'

# Valeport transmits about 34 bytes every 250ms at 115200 baud
AUX_BYTES = 34
AUX_PERIOD = 0.25
BAUDRATE = 115200

TIMEOUT = 1.1 * AUX_PERIOD
MESSAGES = (0.9 * (AUX_PERIOD * 115200 / 10) - AUX_BYTES) // PAYLOAD_MAX

upstream = serial.Serial(UPSTREAM)
aux = serial.Serial(AUX, timeout=TIMEOUT)

upstream_tx_lock = threading.Lock()

shut_down = threading.Event()  # Set to indicate that we are shutting down
aux_tx = threading.Event()  # Set immediately after the an aux transmit happens


def forward_aux():
    """
    Wait for data to be received on the aux port and send it to the upstream
    port. Unidirectional sending of data....

    """

    while not shut_down.is_set():
        aux_in = aux.readline().strip()
        if not aux_in:
            continue
        print aux_in
        with upstream_tx_lock:
            for data in serialise(aux_in, AUX_KEY):
                upstream.write(data)
        aux_tx.set()


def run_camera():
    """
    Wait for transmitted data from upstream - interpret command recieved and
    respond appropriately.

    Currently the supported commands are:
        Snap - take a picture
        Stop = Stop execution
    Currently only really aimed at a Nikon D5200

    """

    setup_cmd = ['gphoto2',  '--set-config', 'capturetarget=1']
    snap_cmd = ['gphoto2', '--capture-image']

    # First set the camera to capture to card... Not RAM (which is default) 
    subprocess.call(setup_cmd)

    # Now we listen to the upstream serial port until something is received.
    while True:
        upstream_in = upstream.readline().strip()
        if upstream_in == 'Snap':
            result = subprocess.check_output(snap_cmd)
            image_paths = []
            for line in result.split('\n'):
                if line:
                    image_paths.append(line)
            send_cam_data(image_paths)
        else:
            send_cam_data(upstream_in)
        if upstream_in == 'Stop':
            break


def send_cam_data(in_ob):
    """
    Helper function to send camera data

    """

    index = 0
    for message in serialise(in_ob, CAM_KEY):
        if (index % MESSAGES) == 0:
            # Wait for aux data....
            aux_tx.clear()
            aux_tx.wait(timeout=TIMEOUT)
        with upstream_tx_lock:
            upstream.write(message)


if __name__ == "__main__":
    # Set up a thread for the forwarder...
    aux_worker = threading.Thread(target=forward_aux)
    aux_worker.start()

    # Nothing else to do, so might as well run the camera here!
    try:
        run_camera()
    except KeyboardInterrupt:
        pass
    finally:
        print 'Shutting Down - Could take up to {}s'.format(TIMEOUT)
        shut_down.set()
        aux_worker.join()
