#!/usr/bin/env python2.7
"""
Python module intended to provide a basic GUI for the stills camera during ROV
deployments.

"""

from Tkinter import Tk, Button, mainloop, SUNKEN, RAISED
from threading import Thread, Event
from serialisation import deserialise, CAM_KEY, AUX_KEY
import serial
import datetime

IN_PORT = r'loop://'
OUT_PORT = r'loop://'
CAMFILE = 'Photolist.txt'
PERIOD = 1  # Interval for pics in seconds


class DownStairs(object):

    def __init__(self, port_url, timeout=10):
        """
        Open up the port, and initialise the generator....

        """

        self.port = serial.serial_for_url(port_url, timeout=timeout)
        self.write = self.port.write
        self.close = self.port.close

    def listener(self):
        """
        Generator that listens on the port described by port_url and generates
        n bytes of data when an integer n is sent to it.

        """

        while True:
            length = yield(None)
            yield(self.port.read(length))


class DisWindow(Thread):
    """
    Class that is intialised with a callback that simply opens a window with a
    single button.

    """

    def __init__(self, callback=None):
        Thread.__init__(self)
        self.callback = callback
        self.state = True

    def run(self):
        top = Tk()
        top.title('Upwards Looking Camera Control')
        self.disable = Button(
            top,
            font=("Helvetica", 24, "bold italic"),
            command=self.update,
            width=20,
        )
        self.update()
        self.disable.pack(expand=4)
        mainloop()

    def update(self):
        self.state = not self.state
        if self.state:
            self.disable.config(
                relief=SUNKEN,
                text='Disable Stills',
                bg='green',
            )
        else:
            self.disable.config(
                relief=RAISED,
                text='Enable Stills',
                bg='red'
            )
        self.callback(self.state)


class CamController(Thread):
    """
    Write to the camera so it takes a photo, but only when enabled.

    """

    def __init__(self, func, args):
        Thread.__init__(self)
        self.stop = Event()
        self.terminate = False
        self.func = func
        self.args = args

    def run(self):
        while True:
            result = self.stop.wait(timeout=PERIOD)
            if result:
                if self.terminate:
                    break
                else:
                    continue
            self.func(*self.args)

    def enable(self):
        self.stop.clear()

    def disable(self):
        self.stop.set()

    def set_state(self, enable=True):
        if enable:
            self.enable()
        else:
            self.disable()

    def kill(self):
        self.terminate = True
        self.stop.set()


def log_cam(data):
    """
    Initially data should just be a string with some info about the captured
    images. This simply writes that to a file with a timestamp

    """

    with open(CAMFILE, 'w') as camlog:
        time_str = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]
        camlog.write(''.join([time_str, ',', data, '\n']))


if __name__ == "__main__":
    ds = DownStairs(IN_PORT)
    aux_port = serial.serial_for_url(OUT_PORT)
    cam = CamController(ds.write, ('snap',))
    stop_button = DisWindow(cam.set_state)
    try:
        cam.start()
        stop_button.start()
        while stop_button.is_alive():
            received = deserialise(ds.listener())
            try:
                cam_data = received[CAM_KEY]
                log_cam(cam_data)
            except KeyError:
                pass
            try:
                aux_data = received[AUX_KEY]
                aux_port.write(aux_data + '\n')
            except KeyError:
                pass
        stop_button.join()
    except:
        raise
    finally:
        ds.close()
        aux_port.close()
        cam.kill()
        cam.join()
