#!/usr/bin/env python2.7
"""
Python module intended to provide basic serialisation of data suitable for
sending over a variety of transports.

"""

from json import dumps, loads
from struct import pack, unpack

PAYLOAD_MAX = 512  # Nice payload size for most transports
AUX_KEY = 'Valeport'
CAM_KEY = 'Camera'

PACK_FORMAT = '<h'


def serialise(input_ob, tag, byte_limit=PAYLOAD_MAX):
    """
    This is a generator that takes an object and a tag and makes a nice serial
    output with it.

    """

    bytes_per = byte_limit - 2
    data = dumps({tag: input_ob})
    index = 0
    while index + bytes_per < len(data):
        this_part = (
            pack(PACK_FORMAT, -bytes_per) + data[index:index + bytes_per]
        )
        yield this_part
        index += bytes_per
    final_part = (
        pack(PACK_FORMAT, len(data[index:])) + data[index:]
    )
    yield final_part
    return


def deserialise(data_stream):
    """
    Intended to do basically the reverse of serialise, given a data stream
    data_stream needs to be a generator that sends n bytes of data when called
    with data_stream.send(n)

    """

    ser_string = ''
    data_stream.next()
    while True:
        temp = data_stream.send(2)
        length, = unpack(PACK_FORMAT, temp)
        data_block = data_stream.send(abs(length))
        if data_block:
            print data_block
            ser_string += data_block
        if length > 0:
            return loads(ser_string)

import unittest
import random


class TestSerialise(unittest.TestCase):

    def setUp(self):
        data_length = 7 * PAYLOAD_MAX + random.randint(1, PAYLOAD_MAX)
        data_str = ''.join(
            [
                chr(x) for x in [
                    random.randint(0, 127) for y in xrange(data_length)
                ]
            ]
        )
        self.in_data = loads(dumps(data_str))

    def serial_gen(self):
        remaining = ''.join(serialise(self.in_data, 'MY_TAG'))
        length = yield(None)
        while True:
            temp = yield(remaining[:length])
            remaining = remaining[length:]
            length = temp

    def test_serialise(self):
        decode = ''
        for block in serialise(self.in_data, 'MY_TAG'):
            length = unpack(PACK_FORMAT, block[0:2])[0]
            decode += block[2:abs(length) + 2]
            if length > 0:
                break
        self.assertEqual(decode, dumps({'MY_TAG': self.in_data}))
        out_data = loads(decode)['MY_TAG']
        self.assertEqual(self.in_data, out_data)

    def test_deserialise(self):
        decode = deserialise(self.serial_gen())
        out_data = decode['MY_TAG']
        self.assertEqual(self.in_data, out_data)

if __name__ == "__main__":
    unittest.main()
